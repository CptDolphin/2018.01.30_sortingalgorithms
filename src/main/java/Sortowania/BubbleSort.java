package Sortowania;

public class BubbleSort {
    public static int[] bubbleSort(int[] tablica) {
        System.out.println("Bubble Sort: ");
        int pom;
        long counter = 0;
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica.length - j; j++) {
                counter++;
                if (tablica[j] > tablica[i]) {
                    pom = tablica[i];
                    tablica[i] = tablica[j];
                    tablica[j] = pom;
                }
            }
        }
        System.out.println("licznik: " + counter);
        return tablica;
    }
}