package Sortowania;

public class CountingSort {
    public static int[] countingSorting(int[] tab, int zakres) {
        int[] tabZakres = new int[zakres + 1];
        long counter = 0;
        for (int i = 0; i < tab.length; i++) {
            counter++;
            tabZakres[tab[i]]++;
        }
        int y = 0;
        for (int i = 0; i < zakres; i++) {
            counter++;
            for (int k = 0; k < tabZakres[i]; k++) {
                tab[y++] = i;
            }
        }
        System.out.println("licznik: " + counter);
        return tab;
    }
}
