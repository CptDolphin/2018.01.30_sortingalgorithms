package Sortowania;

public class InsertionSort {
    public static int[] insertionSort(int[] tab) {
        Long start, stop;
        int counter=0;
        start=System.currentTimeMillis();

        for (int i = 1; i < tab.length - 1; i++) {
            int pom = tab[i];
            int j = i - 1;
            while (j >= 0 && tab[j] > pom) {
                counter++;
                tab[j + 1] = tab[j];
                j --;
            }
            tab[j + 1] = pom;
        }
        stop = System.currentTimeMillis();
        System.out.println("licznik: " + counter);
        System.out.println("Wykonano w: " + (stop-start)+ " milisec");
        return tab;
    }
}
