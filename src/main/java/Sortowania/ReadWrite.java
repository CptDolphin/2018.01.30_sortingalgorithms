package Sortowania;

import java.util.Random;

public class ReadWrite {
    public static final Random random = new Random();

    public static void read(int[] tablica){
        System.out.print("Tablica: ");
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + " ");
        }
        System.out.println();
    }

    public static void write(int[] tablica, int zakres){
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(zakres);
        }
    }
}
