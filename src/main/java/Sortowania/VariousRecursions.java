package Sortowania;

public class VariousRecursions {
    public static void main(String[] args) {
        System.out.println("8 wyraz ciagu fibonaciego: " + fibonacci(8));
        System.out.println("silnia z 6 : " + silnia(6));
        System.out.println(writeFromEnd("Alucard").toLowerCase());
        hanoi(3,'a','b','c');
        System.out.print("Zamiana 10 na binanra: ");
        zamianaNaBinarny(1);
    }

    public static int fibonacci(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        else return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public static int silnia(int n) {
        if (n == 1) return 1;
        else return n * silnia(n - 1);
    }

    public static String writeFromEnd(String word) {
        if (word.length() == 1) return word;
        else {
            char lastLetter = word.charAt(word.length() - 1);
            String stringSubstring = word.substring(0, word.length() - 1);
            return lastLetter + writeFromEnd(stringSubstring);
        }
    }

    public static void hanoi(int n, char a, char b, char c) {
        if (n > 0) {
            hanoi(n - 1, a, c, b);
            System.out.println(a + "->" + c);
            hanoi(n-1,b,a,c);
        }
    }
    public static void zamianaNaBinarny(int n){
        if(n>0){
         zamianaNaBinarny(n/2);
            System.out.print(n%2);
        }
    }
}
