package Sortowania;

public class Merge {
    public static int[] sort(int[] tab, int l, int r) {
        if (l < r) {
            int m = l + (r - l) / 2;
            sort(tab, l, m);
            sort(tab, m + 1, r);
            mergeSort(tab, l, m, r);
        }
        return tab;
    }

    public static void mergeSort(int[] tab, int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;

        int[] L = new int[n1];
        int[] R = new int[n2];

        for (int i = 0; i < n1; i++) {
            L[i] = tab[l + i];
        }
        for (int i = 0; i < n1; i++) {
            R[i] = tab[r + 1];
        }
        int i = 0, j = 0;
        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                tab[k] = L[i];
                i++;
            } else {
                tab[k] = R[j];
                j++;
            }
            k++;
        }
        while (i < n1) {
            tab[k] = L[i];
            i++;
            k++;
        }
        while (j < n2) {
            tab[k] = R[j];
            j++;
            k++;
        }
    }

    public static void mergeWypisanie(int[] tab, int l, int r) {
        Long start, stop;
        start = System.currentTimeMillis();
        sort(tab, l, r);
        stop = System.currentTimeMillis();
        System.out.println("Wykonano w: " + (stop - start / 1000));

    }
}
