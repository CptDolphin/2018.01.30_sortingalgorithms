package Sortowania;


public class Main {
    public static void main(String[] args) {
        int[] tab = new int[15];
        ReadWrite.write(tab, 4);
        ReadWrite.read(tab);
        long start, stop;
        start = System.currentTimeMillis();

//        BubbleSort.bubbleSort(tab);
//        CountingSort.countingSorting(tab, 15);
        InsertionSort.insertionSort(tab);
//        Merge.mergeWypisanie(tab, 0, tab.length - 1);

        stop = System.currentTimeMillis();
        ReadWrite.read(tab);
        System.out.println("Wykonano w : " + (stop - start));
    }
}
